window.addEventListener("load", function() {
    
    let time = 0;
    let nodes = document.querySelectorAll("nav.foldout > a");
    let top = nodes[0].previousElementSibling.offsetHeight;

    for(let i = 0; i < nodes.length; i++) {
        nodes[i].style.transitionDelay = time + "s";
        nodes[i].style.top = top + "px";
        nodes[i].style.zIndex = (nodes.length - i);
        time += 0.2;
        top += nodes[i].offsetHeight;
    }

    let nodes2 = document.querySelectorAll("nav.foldout > *:first-child");
    for(let i = 0; i < nodes2.length; i++) {
        nodes2[i].addEventListener("click", function() {
            this.parentNode.classList.toggle("open");
        });
    }

});